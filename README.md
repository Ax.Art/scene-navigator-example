# Scene Navigator Example
This is an example app (iOS 13.0+, Swift) that demonstrates the use of Coordinators alongside with Scenes and Scene Navigators as part of a MVVM-C (Model-View-View Model + Coordinator) design pattern, in order to manage an app's flow. The app was created as part of the post: http://blog.axart.net/2021/08/04/coordinator-pattern-with-scene-navigator-in-ios/

## How to use
Clone or download the code, open Xcode, build and run.
