//
//  HomeSceneCoordinator.swift
//  Scene Navigator Example
//
//  Created by Achillefs Sourlas on 02.08.21.
//
//  MIT License
//
//  Copyright (c) 2021 Achillefs Sourlas
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit
import Combine

class HomeSceneCoordinator: NSObject, NavigationControllerCoordinator, SceneNavigatorOwner, SceneNavigatorHandler {
    
    var children = [Coordinator]()
    let navigator = SceneNavigator()
    weak var previousNavigationControllerDelegate: UINavigationControllerDelegate?
    
    private let rootViewController: UINavigationController
    private var disposeBag = Set<AnyCancellable>()
    
    required init(rootViewController: UINavigationController) {
        
        self.rootViewController = rootViewController
    }
    
    func start() {
        
        navigator.$route
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [unowned self] in
                handle(scene: $0, source: $1)
            })
            .store(in: &disposeBag)
        
        let homeViewModel = HomeViewModel(title: "Home Scene")
        
        homeViewModel.$event
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [unowned self] in
                self.handle(event: $0)
            })
            .store(in: &disposeBag)
        
        let homeViewController = HomeViewController(viewModel: homeViewModel)
        rootViewController.pushViewController(homeViewController, animated: false)
    }
    
    private func handle(event: HomeViewModel.Event) {
        
        switch event {
            
        case .didTapHelloWorldButton:
            navigator.go(to: .helloWorld(data: HelloWorldSceneNavigationDTO(presentationStyle: .modal)))
        case .didTapDetailSceneButton:
            navigator.go(to: .detail)
        default:
            ()
        }
    }
    
    private func presentDetailScene() {
        
        rootViewController.delegate = self
        
        let coordinator = DetailSceneCoordinator(rootViewController: rootViewController, navigator: navigator)
        coordinator.start()
        children.append(coordinator)
    }
    
    private func presentHomeScene(source: SceneNavigator.Scene) {
        
        switch source {
        
        case .helloWorld(let data):
            
            switch data.presentationStyle {
            
            case .modal:
                rootViewController.dismiss(animated: true, completion: nil)
            
            case .push:
                rootViewController.popToRootViewController(animated: true)
            }
        
        default:
            ()
        }
        
        children.removeAll()
    }
    
    private func presentHelloWorldScene(data: HelloWorldSceneNavigationDTO) {
        
        let coordinator = HelloWorldSceneCoordinator(rootViewController: rootViewController, navigator: navigator, style: data.presentationStyle)
        coordinator.start()
        children.append(coordinator)
    }
    
    // MARK: - SceneNavigatorHandler
    
    func handle(scene: SceneNavigator.Scene, source: SceneNavigator.Scene) {
        
        switch (scene, source) {
        
        case (.detail, .home):
            presentDetailScene()
        
        case (.helloWorld(let data), .home):
            presentHelloWorldScene(data: data)
        
        case (.home, let source):
            presentHomeScene(source: source)
        
        default:
            ()
        }
    }
    
    // MARK: - UINavigationControllerDelegate
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if viewController as? HomeViewController != nil {
            
            navigator.go(to: .home)
        }
    }
}
