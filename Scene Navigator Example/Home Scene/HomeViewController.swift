//
//  HomeViewController.swift
//  Scene Navigator Example
//
//  Created by Achillefs Sourlas on 01.08.21.
//
//  MIT License
//
//  Copyright (c) 2021 Achillefs Sourlas
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit
import Combine

class HomeViewController: UIViewController {
    
    private let viewModel: HomeViewModel
    
    private lazy var detailButton: UIButton = {
        
        let button = UIButton()
        button.setTitle("Go to Detail Scene (Push)", for: .normal)
        button.backgroundColor = .systemBlue
        
        return button
    }()
    
    private lazy var helloWorldButton: UIButton = {
        
        let button = UIButton()
        button.setTitle("Go to Hello World Scene (Modal)", for: .normal)
        button.backgroundColor = .systemRed
        
        return button
    }()
    
    required init(viewModel: HomeViewModel) {
        
        self.viewModel = viewModel
        super.init(nibName: String(describing: HomeViewController.self), bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        title = viewModel.title
        
        detailButton.addTarget(viewModel, action: #selector(HomeViewModel.didTapDetailButton), for: .touchUpInside)
        helloWorldButton.addTarget(viewModel, action: #selector(HomeViewModel.didTapHelloWorldButton), for: .touchUpInside)
        
        let stackView = UIStackView(arrangedSubviews: [detailButton, helloWorldButton])
        view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
        stackView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 16.0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: -16.0).isActive = true
    }
}
