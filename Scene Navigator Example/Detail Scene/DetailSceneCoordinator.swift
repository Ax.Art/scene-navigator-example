//
//  DetailSceneCoordinator.swift
//  Scene Navigator Example
//
//  Created by Achillefs Sourlas on 02.08.21.
//
//  MIT License
//
//  Copyright (c) 2021 Achillefs Sourlas
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit
import Combine

class DetailSceneCoordinator: NSObject, NavigationControllerCoordinator, SceneNavigatorOwner, SceneNavigatorHandler {
    
    var children = [Coordinator]()
    let navigator: SceneNavigator
    weak var previousNavigationControllerDelegate: UINavigationControllerDelegate?
    
    private let rootViewController : UINavigationController
    private var disposeBag = Set<AnyCancellable>()
    
    required init(rootViewController: UINavigationController, navigator: SceneNavigator) {
        
        self.rootViewController = rootViewController
        self.navigator = navigator
    }
    
    func start() {
        
        navigator.$route
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [unowned self] in
                self.handle(scene: $0, source: $1)
            })
            .store(in: &disposeBag)
        
        let detailViewModel = DetailViewModel(title: "Detail Scene (Push)")
        
        detailViewModel.$event
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [unowned self] in
                self.handle(event: $0)
            })
            .store(in: &disposeBag)
        
        let detailViewController = DetailViewController(viewModel: detailViewModel)
        rootViewController.pushViewController(detailViewController, animated: true)
    }
    
    @objc private func didTapCloseButton() {
        
        navigator.go(to: .home)
    }
    
    private func handle(event: DetailViewModel.Event) {
        
        switch event {
        case .didTapHelloWorldButton:
            navigator.go(to: .helloWorld(data: HelloWorldSceneNavigationDTO(presentationStyle: .push)))
        default:
            ()
        }
    }
    
    private func presentHelloWorldScene(data: HelloWorldSceneNavigationDTO) {
        
        previousNavigationControllerDelegate = rootViewController.delegate
        rootViewController.delegate = self
        
        let coordinator = HelloWorldSceneCoordinator(rootViewController: rootViewController, navigator: navigator, style: data.presentationStyle)
        coordinator.start()
        children.append(coordinator)
    }
    
    // MARK: - SceneNavigatorHandler
    
    func handle(scene: SceneNavigator.Scene, source: SceneNavigator.Scene) {
        
        switch (scene, source) {
        
        case (.helloWorld(let data), .detail):
            presentHelloWorldScene(data: data)
            
        case (.detail, .helloWorld):
            rootViewController.delegate = previousNavigationControllerDelegate
            children.removeAll()
            
        default:
            ()
        }
    }
    
    // MARK: - UINavigationcControllerDelegate
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if viewController as? DetailViewController != nil {
            
            navigator.go(to: .detail)
        }
    }
}
