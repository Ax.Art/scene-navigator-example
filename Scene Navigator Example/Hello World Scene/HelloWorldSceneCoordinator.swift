//
//  HelloWorldSceneCoordinator.swift
//  Scene Navigator Example
//
//  Created by Achillefs Sourlas on 02.08.21.
//
//  MIT License
//
//  Copyright (c) 2021 Achillefs Sourlas
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit
import Combine

class HelloWorldSceneCoordinator: Coordinator, SceneNavigatorOwner {
    
    var children = [Coordinator]()
    let navigator: SceneNavigator
    
    private let style: SceneNavigator.ScenePresentationStyle
    private let rootViewController : UINavigationController
    private var disposeBag = Set<AnyCancellable>()
    
    required init(rootViewController: UINavigationController, navigator: SceneNavigator, style: SceneNavigator.ScenePresentationStyle) {
        
        self.rootViewController = rootViewController
        self.navigator = navigator
        self.style = style
    }
    
    func start() {
        
        let title: String
        switch style {
        
        case .modal:
            title = "Hello World Scene (Modal)"
        
        case .push:
            title = "Hello World Scene (Push)"
        }
        
        let helloWorldViewModel = HelloWorldViewModel(title: title, style: style)
        
        helloWorldViewModel.$event
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [unowned self] in
                self.handle(event: $0)
            })
            .store(in: &disposeBag)
        
        let helloWorldViewController = HelloWorldViewController(viewModel: helloWorldViewModel)
        
        switch style {
        
        case .push:
            rootViewController.pushViewController(helloWorldViewController, animated: true)
        
        case .modal:
            let navigationController = UINavigationController(rootViewController: helloWorldViewController)
            rootViewController.present(navigationController, animated: true, completion:nil)
        }
    }
    
    private func handle(event: HelloWorldViewModel.Event) {
        
        switch event {
        
        case .didTapCloseButton:
            navigator.go(to: .home)
            
        default:
            ()
        }
    }
}
