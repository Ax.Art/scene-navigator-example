//
//  HelloWorldViewController.swift
//  Scene Navigator Example
//
//  Created by Achillefs Sourlas on 02.08.21.
//
//  MIT License
//
//  Copyright (c) 2021 Achillefs Sourlas
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit

class HelloWorldViewController: UIViewController {

    lazy var closeButton: UIBarButtonItem = {
        
        UIBarButtonItem(title: "Done", style: .done, target: viewModel, action:#selector(HelloWorldViewModel.didTapCloseButton))
    }()
    
    private lazy var textLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Hello W😎rld!"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 36.0)
        
        return label
    }()
    
    private let viewModel: HelloWorldViewModel
    
    required init(viewModel: HelloWorldViewModel) {
        
        self.viewModel = viewModel
        super.init(nibName: String(describing: HelloWorldViewController.self), bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        title = viewModel.title
        navigationItem.setRightBarButton(closeButton, animated: false)
        
        view.addSubview(textLabel)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 16.0).isActive = true
        textLabel.heightAnchor.constraint(equalToConstant: 128.0).isActive = true
        textLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor, constant: 16.0).isActive = true
        textLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor, constant: -16.0).isActive = true
    }
}
